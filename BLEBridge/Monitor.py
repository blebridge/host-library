#! python3

import argparse
import socket
import threading, time

HOST_NAME = 'localhost'
DEFAULT_PORT = 55311

class Monitor(object):
    """docstring for Monitor"""
    def __init__(self, hostname:str=None, port:int=None):
        super(Monitor, self).__init__()
        
        if not hostname:
            self.hostname = HOST_NAME
        else:
            self.hostname = hostname

        if not port:
            self.port = DEFAULT_PORT
        else:
            self.port = port

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        

    def run(self):
        self._socket.connect((self.hostname, self.port))
        while 1:
            data = self._socket.recv(1024)
            print(self.decode(data), end='')

    def decode(self, data:bytes):
        out = ""

        for d in data:
            if (d > 31 and d < 127) or d == 10 or d == 13:
                out += chr(d)
            else:
                out += "\\x{:02X}".format(d)

        return out


class Sender(object):
    """docstring for Sender"""
    def __init__(self, port:int=0):
        super(Sender, self).__init__()

        self._port = port

        if self._port == -1:
            self._port = DEFAULT_PORT

        self._queue = MessageQueue()
        self._connected = False

    def __del__(self):
        if not self._port:
            return

        # Close socket
        self.Close()

    #---------------------------------------------------------------------------
    ## { function_description }
    ##
    ## :returns:   { description_of_the_return_value }
    ## :rtype:     None
    ##
    ## @todo if program is stopped, last bytes get not flushed to stream.
    ##
    def Close(self) -> None:
        if not self._port:
            return

        # clean up
        self._sock.close()

    def Setup(self) -> None:
        if not self._port:
            return

        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.bind((HOST_NAME, self._port))
        self._thread = threading.Thread(target=self.run, daemon=True)

        print("[Mon] Listening on port {}.".format(self._port))

    def Start(self) -> None:
        if not self._port:
            return

        self._thread.start()

    def Write(self, data:bytes):
        if not self._port:
            return

        self._queue.Write(data)

    def run(self):
        if not self._port:
            return

        self._sock.listen()

        while 1:

            try:
                conn, addr = self._sock.accept()
                self._connected = True
                print("[Mon] Accepted connection from {}".format(addr))

                self._queue.Lock()

                while self._connected:
                    try:
                        data = self._queue.Read()
                        if type(data) == str:
                            data = bytes(data, 'ascii')

                        data += b'\r\n'
                    except:
                        self._queue.Wait(0.5)
                    else:
                        self.send(conn, data)

                self._queue.Unlock()
            except Exception:
                pass
        

    def send(self, c:socket.socket, data:bytes) -> None:
        try:
            c.send(data)
        except Exception as e:
            print("[Mon] Connection lost!")
            self._connected = False


class MessageQueue(object):
    """docstring for MessageQueue"""
    def __init__(self):
        super(MessageQueue, self).__init__()
        self._list = []
        self._cv = threading.Condition()

    def Lock(self):
        self._cv.acquire()

    def Read(self) -> bytes:
        obj = self._list[0]
        self._list.remove(obj)
        return obj

    def Unlock(self) -> None:
        self._cv.release()

    def Wait(self, timeout:int=0):
        return self._cv.wait(timeout)

    def Write(self, data:bytes) -> None:
        try:
            self._cv.acquire()
            self._list.append(data)
        finally:
            self._cv.release()
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Starts a simple socket reader.')
    parser.add_argument('--host', metavar='hostname', type=str, default=HOST_NAME, help='The hostname to connect to.')
    parser.add_argument('-p', '--port', metavar='port', type=int, default=DEFAULT_PORT, help='The TCP port to use.')

    args = parser.parse_args()

    mon = Monitor(args.host, args.port)
    mon.run()

