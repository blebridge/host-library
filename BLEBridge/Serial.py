#! python 3
from datetime import datetime
import serial
import serial.tools.list_ports
import threading, time
from typing import Callable
from . import BridgeExceptions
from . import Monitor
from .Utils import BridgeCommands

class SerialInterface(object):

    #---------------------------------------------------------------------------
    ## Returns the port name of the serial port in use.
    ##
    ## :returns:   the port name.
    ## :rtype:     str
    ##
    @property
    def port(self) -> str:
        return self._stream.port

    #---------------------------------------------------------------------------
    ## Returns the open state of the serial port.
    ##
    ## :returns:   True if open, False otherwise.
    ## :rtype:     boolean
    ##
    @property
    def is_open(self):
        return self._stream.is_open

    #---------------------------------------------------------------------------
    ## Constructs a new instance.
    ##
    ## :param      port:     The serial port to use.
    ## :type       port:     str
    ## :param      monitor:  The network port the monitor should listen on.
    ## :type       monitor:  int
    ##
    def __init__(self, port:str=None, monitor:int=0):
        super(SerialInterface, self).__init__()

        # Setup the serial stream
        self._stream = serial.Serial()
        self._stream.baudrate = 115200
        self._stream_timeout = 0.25                     # 250 ms
        self._stream.timeout = self._stream_timeout
        if port:
            self._stream.port = port

        # empty handler for the reader thread
        self._reader = None

        ##
        ## Reader thread event flags and data structs.
        ##
        self._rb_evt = threading.Event()
        self._resp_evt = threading.Event()
        self._data_evt = threading.Event()
        self._close_evt = threading.Event()
        self._rb_data = None
        self._resp_data = None
        self._data_data = None
        self._evt_list = {}

        # Setup monitoring stream
        self._monitor = Monitor.Sender(port=monitor)
        self._monitor.Setup()
        self._monitor.Start()

    #---------------------------------------------------------------------------
    ## Deletes the object.
    ##
    def __del__(self):
        self.close()

    #---------------------------------------------------------------------------
    ## Closes the serial port and dumps the remaining data to the console.
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    def close(self) -> None:
        # wait for the reader thread to finish.
        self._close_evt.set()
        time.sleep(0.3)

        # dump the remaining content to console.
        print('Buffer dump: {}'.format(self._data_data))

        # close the remote monitoring stream.
        self._monitor.Close()

        # if not yet closed, close the serial port.
        if not self._stream.is_open:
            return

        self._stream.close()

    #---------------------------------------------------------------------------
    ## Opens a the serial port and queries the device status.
    ##
    ## :returns:   whether the device has been openend and provided a valid
    ##             response.
    ## :rtype:     bool
    ##
    def open(self) -> bool:
        # if nothing was bamboozled with, the port should only be open, if we
        # checked the status, so it's the users fault if a non-compatible port
        # was openend.
        if self._stream.is_open:
            return True
        
        # open the port in question
        self._stream.open()
        if not self._stream.is_open:
            return False

        # clear the input, so we can read fresh.
        self._stream.reset_input_buffer()

        # read the status of the device. If this fails, an exception will be
        # raised.
        self.read_status()
        return True

    #---------------------------------------------------------------------------
    ## Reads the data payload following a request.
    ##
    ## :param      timeout:  The timeout in seconds to wait for the data.
    ## :type       timeout:  float
    ##
    ## :returns:   the data that was read.
    ## :rtype:     bytes
    ##
    def read(self, timeout:float=2) -> bytes:
        # let's check if we should do this synchronously:
        if self._reader==None:
            # set the timeout according to the parameter
            self._stream.timeout = timeout
            rdata = bytearray()

            # read data until we're tired.
            while 1:
                rdata += self._stream.read(1)

                # this means the reading operation timed out...
                if not len(rdata):
                    return None
                    break

                # we received the termination character!
                if len(rdata) and rdata[-1] == 0xFA:
                    # read the new-line characters
                    self._stream.read(2)
                    rdata = rdata[:-1]
                    break

            self._stream.timeout = self._stream_timeout

        # or not:
        else:
            # wait for complete data
            if not self._data_evt.wait(timeout):
                return None
            else:
                rdata = self._data_data

        return bytes.fromhex(rdata.decode('ascii'))

    #---------------------------------------------------------------------------
    ## Queries the device status.
    ##
    ## :returns:   the status of the device.
    ## :rtype:     int
    ##
    def read_status(self) -> int:
        err, data = self.write(BridgeCommands.GET_STATUS)
        if err:
            raise BridgeExceptions.DeviceError('Unable to read device status! {}, {}'.format(err, data))

        return int.from_bytes(data, 'little')

    #---------------------------------------------------------------------------
    ## Scans all availabe serial ports for a compatible dongle.
    ##
    ## :returns:   whether a compatible dongle was found.
    ## :rtype:     bool
    ##
    ## @note       If a compatible device was found, the port is closed again
    ## and needs to be opened manually. The serial port however stays set.
    ##
    def scan(self) -> bool:
        # print('Scanning for dongle...')
        ports = serial.tools.list_ports.comports()
        found = False

        for port in ports:
            self._stream.port = port.device

            try:
                if not self.open():
                    raise Exception('')
            except Exception as e:
                self._stream.close()
                self._stream.port = ''
            else:
                found = True
                self._stream.close()
                break

        return found

    #---------------------------------------------------------------------------
    ## Sets the serial port of the module.
    ##
    ## :param      port:          The serial port to use.
    ## :type       port:          str
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    ## :raises     RuntimeError:  if the serial port is already open.
    ##
    def set_port(self, port:str='') -> None:
        if self.is_open:
            raise RuntimeError('Trying to change an open port!')

        self._stream.port = port

    #---------------------------------------------------------------------------
    ## Creates and start the reader thread.
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    ## @note       This function should only be called after the port has been
    ## opened.
    ##
    def start(self) -> None:
        # Create the thread to read and parse data.
        self._reader = threading.Thread(
                                target  = self._t_reader,
                                args    = (),
                                daemon = True )

        self._reader.start()

    #---------------------------------------------------------------------------
    ## Subscribes a callback to a certain characteristic's notifications.
    ##
    ## :param      serv:      The service UUID the characteristic belongs to.
    ## :type       serv:      int
    ## :param      char:      The characteristic UUID to subscribe to.
    ## :type       char:      int
    ## :param      callback:  The callback to be called when the notification is
    ##                        received.
    ## :type       callback:  Function
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    ## @note       The same callback can be subscribed to multiple
    ## characteristics, multiple times. For each call of this function, a call
    ## to @ref unsubscribe has to be made in order to remove the callback
    ## completely.
    ##
    ## @warning:   The callback should not block the execution in any way. If
    ## long operations need to be performed, consider defering it to a separate
    ## worker thread.
    ##
    def subscribe(self, serv:int, char:int, callback:Callable[[bytes], None]) -> None:
        if not serv in self._evt_list:
            self._evt_list[serv] = {}
        if not char in self._evt_list[serv]:
            self._evt_list[serv][char] = []

        self._evt_list[serv][char].append(callback)

    #---------------------------------------------------------------------------
    ## Removes a callback from the subscription to a certain characteristic's
    ## notifications.
    ##
    ## :param      serv:      The service UUID the characteristic belongs to.
    ## :type       serv:      int
    ## :param      char:      The characteristic UUID to unsubscribe from.
    ## :type       char:      int
    ## :param      callback:  The callback to be unsubscribed.
    ## :type       callback:  Function
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    ## @note       For each call to @ref subscribe, a call to this function has
    ## to be made in order to completely remove the callback from a
    ## characteristic's notifications.
    ##
    def unsubscribe(self, serv:int, char:int, callback:Callable[[bytes], None]) -> None:
        if serv in self._evt_list and char in self._evt_list[serv]:
            if callback in self._evt_list[serv][char]:
                self._evt_list[serv][char].remove(callback)

    #---------------------------------------------------------------------------
    ## Writes a command to the dongle.
    ##
    ## :param      cmd:           The command to issue.
    ## :type       cmd:           int
    ## :param      data:          The payload of the command.
    ## :type       data:          bytes
    ## :param      timeout:       The timeout to wait for the response to the
    ##                            command.
    ## :type       timeout:       float
    ## :param      has_data:      Indicates if data is the be expected as a
    ##                            result of a successful command.
    ## :type       has_data:      bool
    ##
    ## :returns:   the error code and any response data associated with the
    ##             command.
    ## :rtype:     int, bytes
    ##
    ## :raises     RuntimeError:  if the command is invalid.
    ##
    def write(self, cmd:int, data:bytes=b'', timeout:float=1, has_data:bool=False) -> [int, bytes]:
        if not cmd in BridgeCommands.COMMAND_DICT:
            raise RuntimeError('Invalid command {:X}'.format(cmd))

        # print(datetime.now().strftime('%S:%f') + " Serial.write: " + BridgeCommands.COMMAND_DICT[cmd])
        # print(" Serial.write: " + BridgeCommands.COMMAND_DICT[cmd])

        # if has_data:
        self._data_evt.clear()

        msg = b'>' + cmd.to_bytes(2, 'big') + bytes(data.hex(), 'ascii') + b'\xFA'

        # self._monitor.Write(datetime.now().strftime('%H:%M:%S:%f') + ": {}".format(msg))
        self._write_monitor("Write", msg)
        self._write(msg, sync=(self._reader==None))

        err, data = self._read_response(timeout=timeout, sync=(self._reader==None))
        if err == -1:
            raise BridgeExceptions.DeviceError('Failed to write {}: {}'.format(BridgeCommands.COMMAND_DICT[cmd], data))

        return err, data

    #---------------------------------------------------------------------------
    ## On receiving a notification, this function executes all callbacks
    ## associated with the characteristic in the order they were subscribed to
    ## it.
    ##
    ## :param      data:  The data associated with the notification.
    ## :type       data:  bytes
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    def _notify(self, data:bytes) -> None:
        serv = int.from_bytes(data[:2], 'little')
        char = int.from_bytes(data[2:4], 'little')

        if serv in self._evt_list and char in self._evt_list[serv]:
            for cb in self._evt_list[serv][char]:
                cb(data[4:])

    #---------------------------------------------------------------------------
    ## Reads the device's response to a request (i.e. error code of it; either
    ## synchronously or asynchronously) from the serial port.
    ##
    ## :param      timeout:  The timeout in seconds to wait for the response.
    ## :type       timeout:  float
    ## :param      sync:     If the data should be read synchronously.
    ## :type       sync:     bool
    ##
    ## :returns:   the error code of the operation and any associated response
    ##             data.
    ## :rtype:     int, bytes
    ##
    def _read_response(self, timeout:float=0.2, sync:bool=False) -> [int, bytes]:
        # if we want to read synchronously:
        if sync:
            # set the timeout according to the parameter
            self._stream.timeout = timeout
            rdata = bytearray()

            # read data until we're tired.
            while 1:
                rdata += self._stream.read(1)

                # this means the reading operation timed out...
                if not len(rdata):
                    rdata = None
                    break

                # we received the termination character!
                if len(rdata) and rdata[-1] == 0xFA:
                    # read the new-line characters
                    self._stream.read(2)
                    rdata = rdata[1:-1]
                    break

            self._stream.timeout = self._stream_timeout
        # otherwise we use the reader thread:
        else:
            # wait for a complete response
            if not self._resp_evt.wait(timeout):
                print('RESP_TIMEOUT')
                rdata = None
            else:
                rdata = self._resp_data
        
        # if we didn't get anything, we're sad:
        if rdata == None:
            return -1, b'\xFF'
        # if we got an OK, forward the status code:
        elif rdata[:3] == b'OK+':
            return 0, bytes.fromhex(rdata[3:].decode('ascii'))
        # if we got an ERR, forward the error code:
        elif rdata[:4] == b'ERR=':
            print('ERROR HERE {}'.format(int(rdata[4:6], 16)))
            return int(rdata[4:6], 16), None
        # otherwise, we're kinda lost...
        else:
            return -1, rdata

    #---------------------------------------------------------------------------
    ## Main entry point for the stream reader thread.
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    def _t_reader(self) -> None:

        #-----------------------------------------------------------------------
        ## Internal function to map the different messages to their
        ## corresponding events.
        ##
        ## :param      data:  The data associated with the event.
        ## :type       data:  bytearray
        ##
        def _reader_parse(data:bytearray):
            # print(datetime.now().strftime('%S:%f') + " Serial._t_reader: {}".format(data))
            # def relay(pref, data):
            #     self._monitor.Write(datetime.now().strftime('%H:%M:%S:%f') + ": [{}]\t{}".format(pref, bytes(data)))

            # Message starts with '>', indicating it is a read-back from a
            # command:
            if data[0] == 0x3E:
                self._write_monitor("Readback", data)
                self._rb_data = data
                self._rb_evt.set()
            # Message starts with '<', indicating it is a response from the
            # device:
            elif data[0] == 0x3C:
                self._write_monitor("Response", data)
                self._resp_data = data[1:-1]
                self._resp_evt.set()
            # Message starts with '!', indicating it is a notification:
            elif data[0] == 0x21:
                self._write_monitor("Notification", data)
                try:
                    self._notify(bytes.fromhex(data[1:-1].decode('ascii')))
                except:
                    self._notify(bytes.fromhex(data[1:9].decode('ascii')) + data[9:])
            # everything else should be payload and expected:
            else:
                self._write_monitor("Payload", data)
                self._data_data = data[:-1]
                self._data_evt.set()
        
        # Let's read data all day long:
        data = bytearray()
        while 1:
            data += self._stream.read(1)

            # if we're supposed to shut down, store the remaining data in the
            # buffer to be dumped.
            if self._close_evt.is_set():
                self._data_data = data
                break

            # if we got a termination character, let's parse it and set the
            # corresponding flags.
            if len(data) and data[-1] == 0xFA:
                self._stream.read(2)
                _reader_parse(data)
                data = bytearray()
    
    #---------------------------------------------------------------------------
    ## Write a raw bytestream to the serial port, either synchronously or
    ## asynchronously.
    ##
    ## :param      data:  The data to be written.
    ## :type       data:  bytes
    ## :param      sync:  Flag indicating if it should happen synchronously or
    ##                    not.
    ## :type       sync:  bool
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    def _write(self, data:bytes, sync:bool=False) -> None:
        # write data to serial port
        self._resp_evt.clear()

        while data:
            self._stream.write(data[:64])
            data = data[64:]
            time.sleep(0.1)

    def _write_monitor(self, kind:str, data:bytes):
        self._monitor.Write(datetime.now().strftime('%H:%M:%S:%f') + ": [{}]\t{}".format(kind, bytes(data)))
