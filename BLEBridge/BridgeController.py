#! python3
import time
import types
from typing import Callable, Union

from . import BridgeExceptions
from . import Monitor
from . import Serial
from . import Utils
from .Utils import BridgeCommands

class BridgeController(object):

    #---------------------------------------------------------------------------
    ## Returns the serial port name.
    ##
    ## :returns:   The port name in use.
    ## :rtype:     str
    ##
    @property
    def serial_port(self) -> str:
        return self._interface.port

    #---------------------------------------------------------------------------
    ## Constructs a new instance.
    ##
    ## :param      comport:  The serial port to use.
    ## :type       comport:  str
    ## :param      kwargs:   The keyword arguments
    ## :type       kwargs:   dictionary
    ##
    def __init__(self, comport:str=None, **kwargs):
        super(BridgeController, self).__init__()

        # Setup the serial interface
        self._interface = Serial.SerialInterface(port=comport, **kwargs)

    #---------------------------------------------------------------------------
    ## Deletes the object.
    ##
    def __del__(self):
        self.close() 

    #---------------------------------------------------------------------------
    ## Should be closed at the end of the program.
    ##
    ## :returns:   nothing
    ## :rtype:     None
    ##
    def close(self) -> None:
        # @todo maybe clean up?
        self._interface.close()

    #---------------------------------------------------------------------------
    ## Connects to a device, either per index, BLE address or DeviceInformation
    ## object.
    ##
    ## :param      device:        The device address, index or information.
    ## :type       device:        int, bytes or Utils.DeviceInformation
    ##
    ## :returns:   whether the connection was successful or not.
    ## :rtype:     bool
    ##
    ## :raises     RuntimeError:  if @p device has the wrong format.
    ##
    def connect(self, device:Union[int, bytes, Utils.BLEAddress, Utils.DeviceInformation]) -> bool:

        # if we use index:
        if type(device) == int:
            print("[BC] Connecting to device with index {}...".format(device))
            err, _ = self._interface.write(BridgeCommands.BLE_CONNECT, bytes([device]))

        # if we use the address:
        elif type(device) == bytes:
            addr = Utils.BLEAddress(device)
            print("[BC] Connecting to device with address {}...".format(addr))
            err, _ = self._interface.write(BridgeCommands.BLE_CONNECT, addr.address)

        # if we use the address struct:
        elif type(device) == Utils.BLEAddress:
            print("[BC] Connecting to device with address {}...".format(device))
            err, _ = self._interface.write(BridgeCommands.BLE_CONNECT, device.address)

        # if we use device information:
        elif type(device) == Utils.DeviceInformation:
            print("[BC] Connecting to device {}...".format(device))
            err, _ = self._interface.write(BridgeCommands.BLE_CONNECT, device.Address.address)

        # or if someone messed up:
        else:
            raise RuntimeError('Bad format for param <device> (must be int, bytes or Utils.DeviceInformation).')

        # if an error was returned, something went wrong.
        if err:
            return False

        # wait until the device completed the request:
        if not self._wait_bit(0, True, 6):
            raise BridgeExceptions.DeviceError('Device busy!')

        # check if we're connected:
        if not self.is_connected():
            return False

        # Construct and store the device information
        if type(device) == int:
            self.ble_device = self.get_device_info(device)
        elif type(device) == bytes or type(device) == Utils.BLEAddress:
            self.ble_device = Utils.DeviceInformation(device)
        else:
            self.ble_device = device

        # query services on the device:
        self.discover_services()

        # and it's characteristics:
        for serv in self.ble_device.ServiceUUIDs:
            self.discover_characteristics(self.ble_device.ServiceUUIDs[serv].UUID)

        return True

    #---------------------------------------------------------------------------
    ## Discovers the characteristics associated with a given service UUID and
    ## stores them in the device information object.
    ##
    ## :param      serv:  The service UUID to scan for characteristics.
    ## :type       serv:  int
    ##
    ## :returns:   nothing
    ## :rtype:     None
    ##
    ## :raises     BridgeExceptions.DeviceError:  if the device doesn't respond.
    ##
    def discover_characteristics(self, serv:int) -> None:
        # first convert the service to bytes.
        serv_bytes = Utils.convert_to_uint16(serv)

        # command the device to scan for characteristics
        err, _ = self._interface.write(BridgeCommands.CHAR_DISC, serv_bytes, timeout=10)
        if err:
            raise BridgeExceptions.DeviceError("Device returned error: {}.".format(data))

        # and wait until it is complete.
        if not self._wait_bit(0, True, 1):
            raise BridgeExceptions.DeviceError("Device busy!")

        # query the results
        err, _ = self._interface.write(BridgeCommands.CHAR_DISC_RES, serv_bytes, timeout=0.5, has_data=True)
        if err:
            raise BridgeExceptions.DeviceError("Could not read discovered characterisitcs: {}.".format(err))

        data = self._interface.read()

        # and store them in the device info struct.
        self.ble_device.ServiceUUIDs[serv].Characteristics = []

        while len(data) > 0:
            uuid = Utils.convert_from_uint16(data[:2])

            if not (uuid in self.ble_device.ServiceUUIDs[serv].Characteristics):
                self.ble_device.ServiceUUIDs[serv].Characteristics.append(uuid)

            data = data[2:]

    #---------------------------------------------------------------------------
    ## Discovers services and adds them to the device information object.
    ##
    ## :param      reset:  Flag indicating if the list of UUIDs should be
    ##                     cleared or the newly found services appended.
    ## :type       reset:  bool
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    def discover_services(self, reset:bool=False) -> None:
        # write the command
        err, _ = self._interface.write(BridgeCommands.SERV_DISC)
        if err:
            raise BridgeExceptions.DeviceError("Device returned error: {}.".format(err))

        # and wait for completion.
        if not self._wait_bit(0, True, 1):
            raise BridgeExceptions.DeviceError("Device still busy!")

        # query the results
        err, _ = self._interface.write(BridgeCommands.SERV_DISC_RES, timeout=0.5, has_data=True)
        if err:
            raise BridgeExceptions.DeviceError("Could not read discovered services: {}.".format(err))

        data = self._interface.read()
        if not data:
            raise BridgeExceptions.DeviceError("Could not read discovered services!")

        # and store them in a newly created or existing dict in the device
        # information object.
        if (self.ble_device.ServiceUUIDs == None) or reset:
            self.ble_device.ServiceUUIDs = {}

        while len(data) > 0:
            uuid = Utils.convert_from_uint16(data[:2])

            if not (uuid in self.ble_device.ServiceUUIDs):
                self.ble_device.ServiceUUIDs[uuid] = Utils.Service(uuid)

            data = data[2:]

    #---------------------------------------------------------------------------
    ## Tries to find an attached dongle connected to one of the available serial
    ## ports.
    ##
    ## :returns:   whether a dongle could be found or not.
    ## :rtype:     bool
    ##
    def find_dongle(self) -> bool:
        return self._interface.scan()

    #---------------------------------------------------------------------------
    ## Gets the device information.
    ##
    ## :param      idx:  The index in the scan-list.
    ## :type       idx:  int
    ##
    ## :returns:   the device information object.
    ## :rtype:     Utils.DeviceInformation
    ##
    def get_device_info(self, idx:int) -> Utils.DeviceInformation:
        err, _ = self._interface.write(BridgeCommands.BLE_GET_DEVICE, bytes([idx]), timeout=0.5, has_data=True)
        if err:
            return None

        data = self._interface.read()
        
        dev = Utils.adv_to_DeviceInformation(data)
        dev.DeviceIndex = idx
        return dev

    #---------------------------------------------------------------------------
    ## Gets the status of the device.
    ##
    ## :returns:   the status of the device.
    ## :rtype:     int
    ##
    def get_status(self) -> int:
        return self._interface.read_status()

    #---------------------------------------------------------------------------
    ## Determines if the device is connected.
    ##
    ## :returns:   True if connected, False otherwise.
    ## :rtype:     bool
    ##
    def is_connected(self) -> bool:
        return Utils.get_bit(self.get_status(), 1)

    #---------------------------------------------------------------------------
    ## Reads a characteristic from service's and characteristic's UUIDs.
    ##
    ## :param      serv:  The service UUID to read from.
    ## :type       serv:  int
    ## :param      char:  The characteristic UUID to read from.
    ## :type       char:  int
    ##
    ## :returns:   the characteristic value.
    ## :rtype:     bytes
    ##
    def read_char(self, serv:int, char:int) -> bytes:
        serv = Utils.convert_to_uint16(serv)
        char = Utils.convert_to_uint16(char)

        err, _ = self._interface.write(BridgeCommands.CHAR_READ, serv + char, timeout=3, has_data=True)
        if err:
            raise BridgeExceptions.DeviceError("Device returned error: {}.".format(err))

        data = self._interface.read()
        return data

    #---------------------------------------------------------------------------
    ## Scan for BLE devices.
    ##
    ## :returns:   the number of devices found.
    ## :rtype:     int
    ##
    def scan(self) -> int:
        print("[BC] Scanning for BLE devices.")

        # Scan for devices.
        res, _ = self._interface.write(BridgeCommands.BLE_SCAN)
        if res:
            raise BridgeExceptions.DeviceError("Device returned error: {}.".format(res))

        # Wait for completion.
        if not self._wait_bit(0, True, 15):
            raise BridgeExceptions.DeviceError("Device still busy!")

        # Read back the number of devices found.
        res, data = self._interface.write(BridgeCommands.BLE_SCAN_RES)
        if res:
            raise BridgeExceptions.DeviceError("Could not read scanned devices: {}.".format(res))

        return int.from_bytes(data, 'little')

    #---------------------------------------------------------------------------
    ## Sets the serial port of the interface.
    ##
    ## :param      port:  The serial port to use.
    ## :type       port:  str
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    def set_port(self, port:str='') -> None:
        self._interface.set_port(port)

    #---------------------------------------------------------------------------
    ## Starts the serial interface, once everything is configured correctly.
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    def start(self) -> None:
        # open the serial port and start the reader thread.
        self._interface.open()
        self._interface.start()

    #---------------------------------------------------------------------------
    ## Subscribe a callback to a characteristic's notifications.
    ##
    ## :param      serv:      The service UUID the characteristic belongs to.
    ## :type       serv:      int
    ## :param      char:      The characteristic UUID to subscribe to.
    ## :type       char:      int
    ## :param      callback:  The callback to be called when a notification is
    ##                        received.
    ## :type       callback:  Function
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    ## @note       The same callback can be subscribed to multiple
    ## characteristics, multiple times. For each call of this function, a call
    ## to @ref unsubscribe has to be made in order to remove the callback
    ## completely.
    ##
    ## @warning:   The callback should not block the execution in any way. If
    ## long operations need to be performed, consider defering it to a separate
    ## worker thread.
    ##
    ## @note       This function can be called without the @p callback just to
    ## activate notifications.
    ##
    def subscribe(self, serv:int, char:int, callback:Callable[[bytes], None]) -> None:
        # Subscribe the callback to the serial reader thread.
        if callback:
            self._interface.subscribe(serv, char, callback)

        # Write the CCCD of the respective characteristic in order to activate
        # notifications.
        serv = Utils.convert_to_uint16(serv)
        char = Utils.convert_to_uint16(char)

        err, _ = self._interface.write(BridgeCommands.CHAR_SUBS, serv + char + b'\x01', timeout=2)
        if err:
            raise BridgeExceptions.DeviceError("Device returned error: {}.".format(err))

    #---------------------------------------------------------------------------
    ## Unsubscribe a callback from the subscription to a characteristic's
    ## notifications.
    ##
    ## :param      serv:      The service UUID the characteristic belongs to.
    ## :type       serv:      int
    ## :param      char:      The characteric UUID to unsubscribe from.
    ## :type       char:      int
    ## :param      callback:  The callback to be unsubscribed.
    ## :type       callback:  Function
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    ## @note       For each call to @ref subscribe, a call to this function has
    ## to be made in order to completely remove the callback from a
    ## characteristic's notifications.
    ##
    ## @note       This function can be called without the @p callback just to
    ## deactivate notifications.
    ##
    def unsubscribe(self, serv:int, char:int, callback:Callable[[bytes], None]=None) -> None:
        # Unsubscribe the callback from the serial reader thread
        if callback:
            self._interface.unsubscribe(serv, char, callback)

        # Write the CCCD of the respective characteristic in order to deactivate
        # notifications.
        serv = Utils.convert_to_uint16(serv)
        char = Utils.convert_to_uint16(char)

        err, _ = self._interface.write(BridgeCommands.CHAR_SUBS, serv + char + b'\x00', timeout=1)
        if err:
            raise BridgeExceptions.DeviceError("Device returned error: {}.".format(err))

    #---------------------------------------------------------------------------
    ## Writes a characteristic value.
    ##
    ## :param      serv:  The service UUID the characteristic belongs to.
    ## :type       serv:  int
    ## :param      char:  The characteristic UUID to be written.
    ## :type       char:  int
    ## :param      data:  The data to be written.
    ## :type       data:  bytes
    ##
    ## :returns:   nothing.
    ## :rtype:     None
    ##
    def write_char(self, serv:int, char:int, data:bytes) -> None:
        serv = Utils.convert_to_uint16(serv)
        char = Utils.convert_to_uint16(char)

        err, _ = self._interface.write(BridgeCommands.CHAR_WRITE, serv + char + data, timeout=5)
        if err:
            raise BridgeExceptions.DeviceError("Could not write characteristic: {}.".format(err))

    #---------------------------------------------------------------------------
    ## Waits for a bit to be either set or cleared.
    ##
    ## :param      idx:      The index of the bit.
    ## :type       idx:      int
    ## :param      clear:    Flag indicating whether the bit should be cleared
    ##                       or not.
    ## :type       clear:    bool
    ## :param      timeout:  The timeout to wait for.
    ## :type       timeout:  int
    ##
    ## :returns:   wheter the corresponding bit assumed the required state.
    ## :rtype:     bool
    ##
    def _wait_bit(self, idx:int, clear:bool=False, timeout:int=15) -> bool:

        while 1:
            status = self.get_status()

            # if bit should be set and is set
            if not clear and Utils.get_bit(status, idx):
                return True
            # if bit should be cleared and is cleared:
            elif clear and not Utils.get_bit(status, idx):
                return True
            # if timeout is over
            elif timeout == 0:
                break
            # else wait
            else:
                timeout -= 1
                time.sleep(1)

        return False

