#! python 3

from dataclasses import dataclass
import threading
from typing import Union

@dataclass
class BridgeCommands(object):
    BLE_SCAN        = 0x0A01
    BLE_SCAN_RES    = 0x0A02
    BLE_GET_DEVICE  = 0x0A03
    BLE_CONNECT     = 0x0A10
    SERV_DISC       = 0x0B01
    SERV_DISC_RES   = 0x0B02
    CHAR_DISC       = 0x0C01
    CHAR_DISC_RES   = 0x0C02
    CHAR_READ       = 0x0C03
    CHAR_WRITE      = 0x0C04
    CHAR_SUBS       = 0x0C05
    GET_STATUS      = 0xF001

    COMMAND_DICT = {
        0x0A01: "BLE_SCAN",
        0x0A02: "BLE_SCAN_RES",
        0x0A03: "BLE_GET_DEVICE",
        0x0A10: "BLE_CONNECT",
        0x0B01: "SERV_DISCOVER",
        0x0B02: "SERV_DISC_RES",
        0x0C01: "CHAR_DISC",
        0x0C02: "CHAR_DISC_RES",
        0x0C03: "CHAR_READ",
        0x0C04: "CHAR_WRITE",
        0x0C05: "CHAR_SUBS",
        0xF001: "GET_STATUS",
    }

AD_DATA_TYPES_DICT = {
    0x01:   "Flags",
    0x02:   "SC_UUID_16_INCP",
    0x03:   "SC_UUID_16_COMP",
    0x04:   "SC_UUID_32_INCP",
    0x05:   "SC_UUID_32_COMP",
    0x06:   "SC_UUID_128_INCP",
    0x07:   "SC_UUID_128_COMP",
    0x08:   "LNAME_SHORT",
    0x09:   "LNAME_COMP",
    0x0A:   "TX_POWER",
    0x0D:   "DEV_CLASS",
    # 0x0E:   
    # 0x0F:   
    0x10:   "DEV_ID"
}

# As found on https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile/ (10.01.2020)
BLE_AD_DATA_TYPES = {
    0x01    :   "Flags",
    0x02    :   "Incomplete List of 16-bit Service Class UUIDs",
    0x03    :   "Complete List of 16-bit Service Class UUIDs",
    0x04    :   "Incomplete List of 32-bit Service Class UUIDs",
    0x05    :   "Complete List of 32-bit Service Class UUIDs",
    0x06    :   "Incomplete List of 128-bit Service Class UUIDs",
    0x07    :   "Complete List of 128-bit Service Class UUIDs",
    0x08    :   "Shortened Local Name",
    0x09    :   "Complete Local Name",
    0x0A    :   "Tx Power Level",
    0x0D    :   "Class of Device",
    0x0E    :   "Simple Pairing Hash C",
    0x0F    :   "Simple Pairing Randomizer R",
    0x10    :   "Device ID"

'''
0x0E    «Simple Pairing Hash C-192»     Core Specification Supplement, Part A, section 1.6
0x0F    «Simple Pairing Randomizer R-192»   Core Specification Supplement, Part A, section 1.6

0x10    «Security Manager TK Value»     Bluetooth Core Specification:Vol. 3, Part C, sections 11.1.7 and 18.6 (v4.0)Core Specification Supplement, Part A, section 1.8
0x11    «Security Manager Out of Band Flags»    Bluetooth Core Specification:Vol. 3, Part C, sections 11.1.6 and 18.7 (v4.0)Core Specification Supplement, Part A, section 1.7
0x12    «Slave Connection Interval Range»   Bluetooth Core Specification:Vol. 3, Part C, sections 11.1.8 and 18.8 (v4.0)Core Specification Supplement, Part A, section 1.9
0x14    «List of 16-bit Service Solicitation UUIDs»     Bluetooth Core Specification:Vol. 3, Part C, sections 11.1.9 and 18.9 (v4.0)Core Specification Supplement, Part A, section 1.10
0x15    «List of 128-bit Service Solicitation UUIDs»    Bluetooth Core Specification:Vol. 3, Part C, sections 11.1.9 and 18.9 (v4.0)Core Specification Supplement, Part A, section 1.10
0x16    «Service Data»  Bluetooth Core Specification:Vol. 3, Part C, sections 11.1.10 and 18.10 (v4.0)
0x16    «Service Data - 16-bit UUID»    Core Specification Supplement, Part A, section 1.11
0x17    «Public Target Address»     Bluetooth Core Specification:Core Specification Supplement, Part A, section 1.13
0x18    «Random Target Address»     Bluetooth Core Specification:Core Specification Supplement, Part A, section 1.14
0x19    «Appearance»    Bluetooth Core Specification:Core Specification Supplement, Part A, section 1.12
0x1A    «Advertising Interval»  Bluetooth Core Specification:Core Specification Supplement, Part A, section 1.15
0x1B    «LE Bluetooth Device Address»   Core Specification Supplement, Part A, section 1.16
0x1C    «LE Role»   Core Specification Supplement, Part A, section 1.17
0x1D    «Simple Pairing Hash C-256»     Core Specification Supplement, Part A, section 1.6
0x1E    «Simple Pairing Randomizer R-256»   Core Specification Supplement, Part A, section 1.6
0x1F    «List of 32-bit Service Solicitation UUIDs»     Core Specification Supplement, Part A, section 1.10
0x20    «Service Data - 32-bit UUID»    Core Specification Supplement, Part A, section 1.11
0x21    «Service Data - 128-bit UUID»   Core Specification Supplement, Part A, section 1.11
0x22    «LE Secure Connections Confirmation Value»  Core Specification Supplement Part A, Section 1.6
0x23    «LE Secure Connections Random Value»    Core Specification Supplement Part A, Section 1.6
0x24    «URI»   Bluetooth Core Specification:Core Specification Supplement, Part A, section 1.18
0x25    «Indoor Positioning»    Indoor Positioning Service v1.0 or later
0x26    «Transport Discovery Data»  Transport Discovery Service v1.0 or later
0x27    «LE Supported Features»     Core Specification Supplement, Part A, Section 1.19
0x28    «Channel Map Update Indication»     Core Specification Supplement, Part A, Section 1.20
0x29    «PB-ADV»    Mesh Profile Specification Section 5.2.1
0x2A    «Mesh Message»  Mesh Profile Specification Section 3.3.1
0x2B    «Mesh Beacon»   Mesh Profile Specification Section 3.9
0x2C    «BIGInfo»   
0x2D    «Broadcast_Code»    
0x3D    «3D Information Data»   3D Synchronization Profile, v1.0 or later
0xFF    «Manufacturer Specific Data»    Bluetooth Core Specification:Vol. 3, Part C, section 8.1.4 (v2.1 + EDR, 3.0 + HS and 4.0)Vol. 3, Part C, sections 11.1.4 and 18.11 (v4.0)Core Specification Supplement, Part A, section 1.4
'''
}

@dataclass
class Service:
    UUID:               int
    Characteristics:    list = None

@dataclass
class BLEAddress:
    address:        bytes

    def __str__(self):
        def hex(val):
            return '{:02X}'.format(val)
        return ':'.join(map(hex, int.from_bytes(self.address, 'little').to_bytes(6, 'big')))

@dataclass
class DeviceInformation:
    Address:        BLEAddress
    DeviceIndex:    int         = None
    Name:           str         = "Unknown"
    DeviceID:       int         = None
    ServiceUUIDs:   dict        = None
    txpower:        int         = None
    devclass:       int         = None

def adv_to_DeviceInformation(data:bytes) -> DeviceInformation:
    dev = DeviceInformation(BLEAddress(data[:6]))
    entries = parse_adv_packet(data[6:])

    dev.ServiceUUIDs = get_service_uuids_from_dict(entries)
    dev.Name = get_name_from_dict(entries)
    dev.DeviceID = get_devid_from_dict(entries)
    dev.txpower = get_txpower_from_dict(entries)
    dev.devclass = get_devclass_from_dict(entries)

    return dev

#-------------------------------------------------------------------------------
## Converts a two byte LE array to a unsigned integer.
##
## :param      data:          The data
## :type       data:          bytes
##
## :returns:   { description_of_the_return_value }
## :rtype:     int
##
## :raises     RuntimeError:  { exception_description }
##
def convert_from_uint16(data:bytes) -> int:
    if len(data) != 2:
        raise RuntimeError("Bad format to convert from 16 bit integer.")

    return int.from_bytes(data, 'little')

def convert_from_uint32(data:bytes) -> int:
    if len(data) != 4:
        raise RuntimeError("Bad format to convert from 32 bit integer.")

    return int.from_bytes(data, 'little')

def convert_to_uint16(val:int) -> bytes:
    if val > 65535:
        raise RuntimeError("Bad value to convert to 16 bit integer.")

    return val.to_bytes(2, 'little')

def convert_to_uint32(val:int) -> bytes:
    if val > 4294967295:
        raise RuntimeError("Bad value to convert to 32 bit integer.")

    return val.to_bytes(4, 'little')

def get_bit(reg:int, idx:int) -> bool:
    return (reg & (1 << idx))

def get_devclass_from_dict(dict_list:list):
    if 'DEV_CLASS' in dict_list:
        return dict_list['DEV_CLASS']
    else:
        return None

def get_devid_from_dict(dict_list:list):
    if 'DEV_ID' in dict_list:
        return dict_list['DEV_ID']
    else:
        return None

def get_name_from_dict(dict_list:list) -> str:
    if 'LNAME_COMP' in dict_list:
        return dict_list['LNAME_COMP']
    elif 'LNAME_SHORT' in dict_list:
        return dict_list['LNAME_SHORT']
    else:
        return 'Unknown'

def get_service_uuids_from_dict(dict_list:list) -> list:
    uuids = {}
    if 'SC_UUID_16_COMP' in dict_list:
        nUUIDs = int(len(dict_list['SC_UUID_16_COMP'])/2)
        for i in range(nUUIDs):
            cur_uuid = convert_from_uint16( dict_list['SC_UUID_16_COMP'][2*i:2*(i+1)])
            uuids[cur_uuid] = Service(cur_uuid)

    elif 'SC_UUID_16_INCP' in dict_list:
        nUUIDs = int(len(dict_list['SC_UUID_16_INCP'])/2)
        for i in range(nUUIDs):
            cur_uuid = convert_from_uint16( dict_list['SC_UUID_16_INCP'][2*i:2*(i+1)])
            uuids[cur_uuid] = Service(cur_uuid)

    return uuids

def get_txpower_from_dict(dict_list:list):
    if 'TX_POWER' in dict_list:
        return dict_list['TX_POWER']
    else:
        return None


#-------------------------------------------------------------------------------
## Parse an advertisement or scan-response packet and create a dict out of the
## payload.
##
## :type       data:  bytes
## :param      data:  The data
##
## :returns:   { description_of_the_return_value }
## :rtype:     dict
##
def parse_adv_packet(data:bytes) -> dict:

    out = {}

    while 1:
        if (len(data) == 0) or (data[0] == 0):
            break
        elif len(data) < (data[0] + 1):
            raise RuntimeError('Invalid advertisement data!')
        else:
            end = data[0] + 1
            key, val = parse_adv_segment(data[1:end])
            out[key] = val

            data = data[end:]

    return out

#-------------------------------------------------------------------------------
## Parse a data segment of an advertisement or scan-response packet.
##
## :type       data:  bytes
## :param      data:  The data
##
## :returns:   { description_of_the_return_value }
## :rtype:     { return_type_description }
##
def parse_adv_segment(data:bytes) -> (Union[int, str], Union[bytes, str, list]):

    # parse 16-bit Service UUID
    if (data[0] == 0x02 or data[0] == 0x03):
        return AD_DATA_TYPES_DICT[data[0]], data[1:]
    # parse name
    elif (data[0] == 0x08 or data[0] == 0x09) and not (data[1] == 0):
        return AD_DATA_TYPES_DICT[data[0]], data[1:].decode()
    

    return data[0], data[1:]

class MessageQueue(object):
    """docstring for MessageQueue"""
    def __init__(self, length:int=None):
        super(MessageQueue, self).__init__()
        self._list = []
        self._cv = threading.Condition()
        self._length = length

    def Lock(self):
        self._cv.acquire()

    def Read(self) -> bytes:
        obj = self._list[0]
        self._list.remove(obj)
        return obj

    def Unlock(self) -> None:
        self._cv.release()

    def Wait(self, timeout:int=0):
        return self._cv.wait(timeout)

    def Write(self, data:bytes) -> None:
        try:
            self._cv.acquire()

            if self._length and not (len(self._list) < self.length):
                # remove oldest entry and append
                self.Read()
                self._list.append(data)

            else:
                self._list.append(data)
        finally:
            self._cv.release()
