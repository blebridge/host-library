#! python3

import argparse
from BLEBridge import BridgeController
from BLEBridge import Utils
import time

TEST_COM_PORT_DETECTION = 0
TEST_SCANNING = 0
TEST_READ_CHAR = 0
TEST_WRITE_CHAR = 0
TEST_NOTIFICATIONS = 0

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Host application for the BLEBridge interface.')
    parser.add_argument('-p', '--port', metavar='port', type=str, help='The serial port to use.')
    parser.add_argument('-m', '--monitor', metavar='monitor', type=int, nargs='?', default=0, help='Relay incoming data to a TCP port.')

    args = parser.parse_args()

    # Map the monitor argument
    if args.monitor == None:
        args.monitor = -1

    return args


if __name__ == '__main__':

    args = parse_args()
    bridge = BridgeController.BridgeController(monitor=args.monitor)

    ##
    ## Test the COM port detection and opening mechanism
    ##
    if TEST_COM_PORT_DETECTION:
        if not args.port:
            if bridge.find_dongle():
                print('Found dongle at {}'.format(bridge.serial_port))
            else:
                print('No compatible dongle found!')
                bridge.close()
                exit()
    else:
        bridge.set_port(args.port)

    bridge.start()
    print('Bridge started. Status: {}'.format(bridge.get_status()))

    ##
    ## Test the scanning mechanism
    ##
    if TEST_SCANNING:
        print('Scanning...')
        n_devices = bridge.scan()
        print("Found {} devices".format(n_devices))

        device = None
        device_idx = -1

        for i in range(0, n_devices):
            data = bridge.get_device_info(i)
            if data.Name == 'nRF Connect':
                device_idx = i
                device = data
                print("Found nRF52840!")
                break

        if device == None:
            print('Could not find test device!')
            exit()
    else:
        device = Utils.DeviceInformation(b'\xB2\x4A\xAB\x21\xDF\xEF')


    #---------------------------------------------------------------------------
    ## Connect to the device
    ##
    if bridge.connect(device):
        print("Connected to {}!".format(device.Address))
    else:
        print("failed to connect")
        bridge.close()
        exit()

    for service in bridge.ble_device.ServiceUUIDs:
        print("Service:    0x{:04X}".format(service))
        for char in bridge.ble_device.ServiceUUIDs[service].Characteristics:
            print("    Char:       0x{:04X}".format(char))

    ##
    ## Test the reading mechanism
    ##
    if TEST_READ_CHAR:
        for i in range(0, 10):
            print("Reading characteristic:")
            res = bridge.read_char(0x5100, 0x5101)
            print("Val = {}".format(res))
            time.sleep(1)

    ##
    ## Test the reading mechanism
    ##
    if TEST_WRITE_CHAR:
        res = bridge.write_char(0x5100, 0x5101, b'\xC0\xFE')
        print('Wrote: {}'.format(res))

    ##
    ## Test the notidication mechanisms
    ##
    if TEST_NOTIFICATIONS:

        def cb_HR(data):
            print('HR: {}'.format(data))

        def cb_SpO2(data):
            print('SpO2: {}'.format(data))

        def cb_ACC(data):
            print('ACC: {}'.format(data))

        bridge.subscribe(0x5100, 0x5111, cb_HR)
        bridge.subscribe(0x5100, 0x5112, cb_SpO2)
        bridge.subscribe(0x5100, 0x5116, cb_ACC)

        print('Starting countdown...')
        time.sleep(60)

    ##
    ## Terminate execution
    ##
    bridge.close()
    exit()
