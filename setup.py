import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="BLEBridge",
    version="0.1",
    author="Yvan Bosshard",
    author_email="byvan@ethz.ch",
    description="Open Source BLE bridge library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.ethz.ch/blebridge/host-library",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=['pyserial==3.4'],
    python_requires='>=3.7',
)